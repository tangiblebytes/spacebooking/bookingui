# Release Process

Making it manual to start with - aiming to automate later

1. Build and Test
1. pushing from local so make sure it is clean
1. `` (or major, or patch)
1. Go to https://gitlab.com/tangiblebytes/spacebooking/bookingui/-/releases
1. Create a new release with version number as name
1. Add a changelog and any useful notes
1. Create sentry release

```bash

export VERSION=$(npm version minor)
# export verion number and relaese name
git push && git push --tags
npm run build
sentry-cli releases new rockburn-booking@$VERSION
sentry-cli releases set-commits "rockburn-booking@$VERSION" --auto
sentry-cli releases files rockburn-booking@$VERSION upload-sourcemaps ./build/
sentry-cli releases finalize rockburn-booking@$VERSION
sentry-cli releases deploys rockburn-booking@2.0.0 new --env production --name $NAME
firebase deploy -P default`
```

1. `

## For future work

Gitlab release automation

https://gitlab.com/groups/gitlab-org/-/epics/2510

https://about.gitlab.com/blog/2020/05/07/how-gitlab-automates-releases/
