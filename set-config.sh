#!/bin/bash
firebase use default
firebase functions:config:set env.name="production"
firebase use staging
firebase functions:config:set env.name="staging"
