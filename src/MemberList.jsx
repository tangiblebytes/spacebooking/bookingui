import React, { useState, useEffect } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";

function MemberList(props) {
  const db = props.db;
  const user = props.user; //in App state
  // const profile = props.profile; //in App state
  const [members, setMembers] = useState([]);

  useEffect(() => {
    if (user) {
      let data = [];
      db.collection("users")
        .get()
        .then(function (querySnapshot) {
          querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots

            data.push({
              uid: doc.id,
              email: doc.data().email,
              name: doc.data().name,
              phone: doc.data().phone,
              address: doc.data().address,
              dob: doc.data().dob,
              member: doc.data().member,
              inducted: doc.data().inducted,
              completedForm: doc.data().completedForm,
            });
          });
          return data;
        })
        .then((data) => {
          setMembers(data);
        })
        .catch(function (error) {
          console.log("Error getting documents: ", error);
        });
    }
  }, [user, db]);

  const columns = [
    {
      dataField: "uid",
      text: "User ID",
      hidden: true,
    },

    {
      dataField: "name",
      text: "Name",
      sort: true,
    },
    {
      dataField: "email",
      text: "email",
      sort: true,
    },
    {
      dataField: "phone",
      text: "Phone",
      sort: true,
    },
    {
      dataField: "address",
      text: "Address",
      sort: true,
    },

    {
      dataField: "dob",
      text: "DOB",
      sort: true,
    },
    {
      dataField: "member",
      text: "Member",
      sort: true,
    },
    {
      dataField: "inducted",
      text: "Inducted",
      sort: true,
    },
    {
      dataField: "completedForm",
      text: "Form",
      sort: true,
    },
  ];

  return (
    <div>
      <BootstrapTable
        bootstrap4
        keyField="uid"
        data={members}
        columns={columns}
        sort={{ dataField: "name", order: "asc" }}
      />
    </div>
  );
}

export default MemberList;
