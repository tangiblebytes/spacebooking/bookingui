import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
// import * as firebase from "firebase/app";
import firebase from "firebase/app";
import { useHistory } from "react-router-dom";
import getProfile from "./Profile";

function ProfileForm(props) {
  const db = props.db;
  const user = props.user; //in App state

  const history = useHistory();

  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [dob, setDob] = useState("");
  const [member, setMember] = useState(false);
  const [inducted, setInducted] = useState(false);
  const [completedForm, setCompletedForm] = useState(false);

  useEffect(() => {
    if (user) {
      getProfile(db, user)
        .then((profile) => {
          setName(profile.name);
          setPhone(profile.phone);
          setAddress(profile.address);
          setDob(profile.dob);
          setMember(profile.member);
          setInducted(profile.inducted);
          setCompletedForm(profile.completedForm);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    // eslint-disable-next-line
  }, []);

  let handleSubmit = (event) => {
    const userData = {
      name: name,
      email: user.email,
      phone: phone,
      address: address,
      dob: dob,
      member: member,
      inducted: inducted,
      completedForm: completedForm,
      created: firebase.firestore.FieldValue.serverTimestamp(), // TODO - only set on create
      updated: firebase.firestore.FieldValue.serverTimestamp(),
    };

    db.collection("users")
      .doc(user.uid)
      .set(userData)
      .then(() => {
        var user = firebase.auth().currentUser;
        return user.updateProfile({ displayName: name });
      })
      .then(() => {
        //  console.log("Document successfully written!");
        history.push({ pathname: "/" });
      })
      .catch(function (error) {
        // TODO Handle Errors here.
        console.log(error);
      });
    event.preventDefault();
  };

  return (
    <div className="container">
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="signUpFormName">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="name"
            placeholder=""
            value={name}
            required
            onChange={(e) => setName(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="signUpFormPhone">
          <Form.Label>Phone</Form.Label>
          <Form.Control
            type="tel"
            placeholder=""
            value={phone}
            required
            onChange={(e) => setPhone(e.target.value)}
          />
          <Form.Text className="text-muted">
            In case we need to check something with you or alter a booking.
          </Form.Text>
        </Form.Group>
        <Form.Group controlId="signUpFormAddress">
          <Form.Label>Address</Form.Label>
          <Form.Control
            as="textarea"
            rows="3"
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />

          <Form.Text className="text-muted">
            In case we need to check something with you or alter a booking.
          </Form.Text>
        </Form.Group>
        <Form.Group controlId="signUpFormDob">
          <Form.Label>Date of Birth</Form.Label>
          <Form.Control
            type="date"
            placeholder=""
            value={dob}
            onChange={(e) => setDob(e.target.value)}
          />
        </Form.Group>

        <Form.Check
          type="checkbox"
          label="Current Member"
          id="signUpFormCurrentMember"
          checked={member}
          onChange={(e) => setMember(e.target.checked)}
        />

        <Form.Check
          type="checkbox"
          label="Completed Individual Climber Form"
          id="signUpFormCompletedForm"
          checked={completedForm}
          onChange={(e) => setCompletedForm(e.target.checked)}
        />

        <Form.Check
          type="checkbox"
          label="Rockburn Inducted "
          id="signUpFormInducted"
          checked={inducted}
          onChange={(e) => setInducted(e.target.checked)}
        />
        <p>
          <Button variant="primary" type="submit">
            Save Profile
          </Button>{" "}
          <Button variant="secondary" href="/">
            Cancel
          </Button>
        </p>
      </Form>
    </div>
  );
}

export default ProfileForm;
