import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/Alert";
// import firebase from "firebase/app";
// import * as firebase from "firebase/app";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";

function SignInForm(props) {
  const auth = props.auth;

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const history = useHistory();

  const showError = (message) => {
    setErrorMessage(message);
    setShowErrorMessage(true);
  };

  const signIn = (event) => {
    auth
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        history.push({ pathname: "/" });
      })
      .catch((error) => {
        if (error.code && error.message) {
          if (error.code === "auth/user-not-found") {
            showError("login Failed");
          } else {
            showError(error.message);
          }
        } else {
          showError("Something went wrong");
        }
      });
    event.preventDefault();
  };

  return (
    <div className="container">
      <Alert show={showErrorMessage} variant="warning">
        {errorMessage}
      </Alert>
      <Form onSubmit={signIn}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Sign In
        </Button>
      </Form>
      <p>&nbsp;</p>
      <p>
        <Link to="/reset">Request Password Reset</Link>{" "}
      </p>
    </div>
  );
}

export default SignInForm;
