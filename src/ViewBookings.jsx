import React, { useState, useEffect } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import moment from "moment";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import CancelButton from "./CancelButton";
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";

function ViewBookings(props) {
  const db = props.db;
  const functions = props.functions;
  const user = props.user; //in App state
  const [bookings, setBookings] = useState([]);
  const [past, setPast] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    if (user) {
      let searchOption = past ? "<" : ">";
      let data = [];
      const startOfToday = new Date(new Date().setHours(0, 0, 0, 0));
      db.collection("userBookings")
        .where("booking.date", searchOption, startOfToday)
        .get()
        .then(function (querySnapshot) {
          querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            let date = doc.data().booking.date.toDate();
            let cancelledTime = {};
            if (doc.data().cancelTime) {
              cancelledTime = doc.data().cancelTime.toDate();
            }
            data.push({
              bookingID: doc.id,
              eventID: doc.data().eventID,
              timestamp: moment(date).format("YYYY-MM-DD-HH-mm"),
              date: moment(date).format("dddd Do MMMM YYYY"),
              dateDate: date,
              created: moment(doc.data().booking.created.toDate())
                .format("YYYY-MM-DD HH:mm"),
              time: moment(date).format("h:mm a"),
              uid: doc.data().booking.uid,
              name: doc.data().booking.name,
              cancelled: doc.data().cancelled,
              cancelledBy: doc.data().cancelledBy,
              cancelledTime: moment(cancelledTime).format("MM-DD HH:mm"),
            });
          });
          return data;
        })
        .then((data) => {
          setBookings(data);
          setLoading(false);
        })
        .catch(function (error) {
          console.log("Error getting documents: ", error);
        });
    }
  }, [user, db, past]);

  const columns = [
    {
      dataField: "name",
      text: "Name",
      sort: true,
    },
    {
      dataField: "id",
      text: "Booking ID",
      hidden: true,
    },
    {
      dataField: "dateDate",
      hidden: true,
    },
    {
      dataField: "cancelledBy",
      hidden: true,
    },
    {
      dataField: "cancelledTime",
      hidden: true,
    },
    {
      dataField: "timestamp",
      text: "Date",
      formatter: (cell, row, rowIndex) => {
        return row.date; // sort by timestamp but display date
      },
      sort: true,
    },

    {
      dataField: "time",
      text: "Time",
      sort: true,
    },
    {
      dataField: "created",
      text: "Booking Time",
      sort: true,
    },
    {
      dataField: "cancelled",
      text: "Cancel",
      sort: true,
      formatter: (cell, row, rowIndex) => {
        //turn cell;
        if (cell)
          return "Cancelled by " + row.cancelledBy + " " + row.cancelledTime;
        else
          return (
            <CancelButton
              functions={functions}
              bookingName={row.name}
              bookingDate={row.dateDate}
              bookingID={row.bookingID}
            />
          );
      },
    },
  ];

  let pastFuture = past ? "Show Future Bookings" : "Show Past Bookings";

  return (
    <div className="container">
      <div className="my-4">
        <Button
          onClick={() => {
            setPast(!past);
          }}
        >
          {pastFuture}
        </Button>
      </div>
      {loading ? (
        <div>
          <Spinner animation="border" /> Loading ...
        </div>
      ) : (
        <BootstrapTable
          bootstrap4
          keyField="id"
          data={bookings}
          columns={columns}
          sort={{ dataField: "timestamp", order: "asc" }}
        />
      )}
    </div>
  );
}

export default ViewBookings;
