const getProfile = (db, user) => {
  const profile = {
    name: user.email,
    phone: "",
    address: "",
    dob: "",
    member: "",
    inducted: "",
    completedForm: "",
    isAdmin: false, // only use admin property for UI - eg which links to show - real security is in rules
  };
  return new Promise((resolve, reject) => {
    var docRef = db.collection("users").doc(user.uid);
    docRef
      .get()
      .then((doc) => {
        if (doc.exists) {
          profile.name = doc.data().name;
          profile.phone = doc.data().phone;
          profile.address = doc.data().address;
          profile.dob = doc.data().dob;
          profile.member = doc.data().member;
          profile.inducted = doc.data().inducted;
          profile.completedForm = doc.data().completedForm;
        } else {
          // don't reject - new users have no profile
          // reject("User not found");
        }
        return;
      })
      .then(() => {
        return db.collection("roles").doc(user.uid).get();
      })
      .then((doc) => {
        if (doc.exists && doc.data().role && doc.data().role.isAdmin) {
          profile.isAdmin = true;
        }
        resolve(profile);
      })
      .catch(function (error) {
        reject(error);
      });
  });
};

export default getProfile;
