import React, { useState, useEffect } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick

import EventModal from "./EventModal";
import EventAction from "./EventAction";

import moment from "moment";
import "firebase/firestore";
import "firebase/functions";

function BookingApp(props) {
  const db = props.db;
  const functions = props.functions;
  const user = props.user;
  const profile = props.profile;
  const [myBookings, setMyBookings] = useState([]);
  const [lastBooking, setLastBooking] = useState(Date.now()); // used to trigger refresh of bookings when user makes a new booking
  const [showEventAction, setShowEventAction] = useState(false);
  const [eventMode, setEventMode] = useState("add");
  useEffect(() => {
    let bookings = [];
    if (user && user.uid) {
      db.collection("userBookings")
        .where("booking.uid", "==", user.uid)
        .get()
        .then(function (querySnapshot) {
          querySnapshot.forEach(function (doc) {
            if (!doc.data().cancelled) {
              bookings.push(doc.data().eventID);
            }
          });
          return bookings;
        })
        .then((bookings) => {
          // console.log(bookings);
          setMyBookings(bookings);
        })
        .catch(function (error) {
          console.error("Error getting documents: ", error);
        });
    }

    // second arg to useEffect is an array of dependencies (when the hook should be triggered)
    // because db variable is used the linter thinks this is a depency
    // but we don't want update if something on the db changes
    // eslint-disable-next-line
  }, [user, lastBooking]);

  const calendarWeekends = true;
  const [calendarEvents, setCalendarEvents] = useState([]);
  const [bookingEvent, setBookingEvent] = useState({
    extendedProps: { rawTitle: "" },
  });
  // const [showEvent, setShowEvent] = useState(false);
  const [dateInfo, setDateInfo] = useState({});

  // const [alreadyBooked, setAlreadyBooked] = useState("");
  const [newEvent, setNewEvent] = useState({});
  const [showAddEvent, setShowAddEvent] = useState(false);

  const updateCalender = () => {
    getCalendarEvents(dateInfo, db, setCalendarEvents);
  };

  const eventClick = (info) => {
    if (
      !info ||
      !info.event ||
      info.event.extendedProps.booked >= info.event.extendedProps.capacity // It's fully booked
    ) {
      return;
    }
    const event = info.event;

    let now = new Date();
    if (event.start < now) return; // TODO fix timezone/BST issues - not critical

    setBookingEvent(event);
    setShowEventAction(true);
  };

  const addEvent = (date) => {
    if (date.date < new Date() || !profile.isAdmin) {
      return;
    }
    setNewEvent(date.date);
    setShowAddEvent(true);
    setEventMode("add");
  };

  const showEditEvent = () => {
    setShowEventAction(false);
    setShowAddEvent(true);
    setEventMode("edit");
  };

  const dayHeaderFormat = { weekday: "short", day: "numeric" };

  return (
    <>
      <div className="container">
        {!user && (
          <div>
            <p>
              You need to register to use Rockburn's online booking system -
              please <a href="/register">register</a> if this is your first
              visit.
            </p>
            <p>
              Otherwise <a href="/signin">sign in</a> and book your times.{" "}
            </p>
          </div>
        )}
        <p>Please book before your visit to ensure availability.</p>

        <FullCalendar
          initialView="timeGridWeek"
          headerToolbar={{
            left: "prev,next today",
            center: "title",
            right: "timeGridWeek,timeGridDay",
            eventDurationEditable: false,
            //editable: true,
          }}
          dayHeaderFormat={dayHeaderFormat}
          selectable={true}
          firstDay="1"
          timeZone="UTC"
          // slotDuration="01:00:00"
          slotMinTime="10:00:00"
          slotMaxTime="22:00:00"
          expandRows="true"
          allDaySlot={false}
          height="60em"
          longPressDelay="100"
          plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
          //  ref={calendarComponentRef}
          weekends={calendarWeekends}
          events={calendarEvents}
          dateClick={addEvent}
          eventClick={eventClick}
          datesSet={(dateInfo) => {
            setDateInfo(dateInfo);
            getCalendarEvents(dateInfo, db, setCalendarEvents);
          }}
        />
        <EventModal
          show={showAddEvent}
          setShow={setShowAddEvent}
          eventMode={eventMode}
          updateCalender={updateCalender}
          bookingEvent={bookingEvent}
          date={newEvent}
          db={db}
        />
        <EventAction
          functions={functions}
          show={showEventAction}
          setShow={setShowEventAction}
          showEditEvent={showEditEvent}
          updateCalender={updateCalender}
          event={bookingEvent}
          myBookings={myBookings}
          user={user}
          setLastBooking={setLastBooking}
          isAdmin={profile.isAdmin}
        />
      </div>
    </>
  );
}

function getCalendarEvents(dateInfo, db, callback) {
  var events = [];
  db.collection("events")
    .where("start", ">=", dateInfo.start)
    .where("start", "<=", dateInfo.end)
    .get()
    .then(function (querySnapshot) {
      querySnapshot.forEach((doc) => {
        const event = doc.data();
        let colour = "blue";
        let available = event.capacity - event.booked;
        if (event.start.toDate() < new Date()) {
          colour = "grey";
        } else if (available <= 0) {
          colour = "orange";
        } else if (event.booked > 0) {
          colour = "paleblue";
        }
        let calenderEvent = {
          id: doc.id,
          title: event.title + " (" + available + " spaces)",
          start: moment(event.start.toDate()).format("YYYY-MM-DD HH:mm:ss"),
          end: moment(event.end.toDate()).format("YYYY-MM-DD HH:mm:ss"),
          color: colour,
          extendedProps: {
            booked: event.booked,
            capacity: event.capacity,
            rawTitle: event.title,
          },
        };
        events.push(calenderEvent);
      });
      callback(events);
    })
    .catch(function (error) {
      console.error("Error getting documents: ", error); // TODO display error
    });
}

export default BookingApp;
