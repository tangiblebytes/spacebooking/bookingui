import React from "react";
import EventModal from "../../EventModal.jsx";

import { mount } from "enzyme";

let showAddEvent = true;
let setShowAddEvent = (value) => {
  showAddEvent = value;
};
let updateCalender = jest.fn();
let newEvent = new Date("2030-01-01 15:00");
let db = jest.fn();

test("contains expected text", () => {
  const wrapper = mount(
    <EventModal
      show={true}
      setShow={setShowAddEvent}
      eventMode="add"
      updateCalender={updateCalender}
      date={newEvent}
      db={db}
    />
  );

  expect(wrapper.text()).toMatch("New Event");
  expect(wrapper.find("input#date").instance().value).toBe("2030-01-01");
  expect(wrapper.find("input#title").instance().value).toBe("climbing");
  expect(wrapper.find("input#start").instance().value).toBe("15:00");
  expect(wrapper.find("input#end").instance().value).toBe("16:00");
  expect(wrapper.find("input#capacity").instance().value).toBe("15");
  expect(db).toHaveBeenCalledTimes(0);
  expect(showAddEvent).toBe(true);
});

test("closes", () => {
  const wrapper = mount(
    <EventModal
      show={true}
      setShow={setShowAddEvent}
      eventMode="add"
      updateCalender={updateCalender}
      date={newEvent}
      db={db}
    />
  );
  wrapper.find({ children: "Cancel" }).find("button").simulate("click");
  expect(db).toHaveBeenCalledTimes(0);
  expect(showAddEvent).toBe(false);
  showAddEvent = true;
});

test("Change Date", () => {
  const wrapper = mount(
    <EventModal
      show={true}
      setShow={setShowAddEvent}
      eventMode="add"
      updateCalender={updateCalender}
      date={newEvent}
      db={db}
    />
  );

  wrapper
    .find("input#date")
    .simulate("change", { target: { value: "2030-01-02" } });
  expect(wrapper.text()).toMatch("Wednesday January 2nd");
  expect(wrapper.find("input#title").instance().value).toBe("climbing");
  expect(wrapper.find("input#start").instance().value).toBe("15:00");
  expect(wrapper.find("input#end").instance().value).toBe("16:00");
  expect(wrapper.find("input#capacity").instance().value).toBe("15");
  expect(db).toHaveBeenCalledTimes(0);
  expect(showAddEvent).toBe(true);
});
