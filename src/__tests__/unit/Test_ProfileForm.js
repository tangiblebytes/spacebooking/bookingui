import ProfileForm from "../../ProfileForm";
import { shallow } from "enzyme";

// require("firebase/firestore");

// firebase.initializeApp(firebaseConfig);
// const db = firebase.firestore();

// const auth = firebase.auth();
// if (window.location.hostname === "localhost") {
// db.settings({
//   host: "localhost:8080",
//   ssl: false,
// });
// auth.useEmulator("http://localhost:9099/");
// }
const db = {};
const user = { uid: "user1", email: "sean@test.com" };
describe("<ProfileForm> with props", () => {
  const container = shallow(<ProfileForm db={db} user={user} />);

  it("should match the snapshot", () => {
    expect(container.html()).toMatchSnapshot();
  });

  it("should have a name field", () => {
    // expect(container.find('input[id="signUpFormName"]').length).toEqual(1);
    expect(container.find('FormControl[type="name"]').length).toEqual(1);
    expect(
      container.find('FormGroup[controlId="signUpFormName"]').length
    ).toEqual(1);
  });

  //   console.log(container.debug());

  // it("calls useEffect", () => {
  //   sinon.spy(ProfileForm.useEffect);
  //   mount(<ProfileForm db={db} user={user} />);
  //   console.log(ProfileForm.useEffect.debug());
  //   expect(ProfileForm.useEffect).to.have.property("callCount", 1);
  // });

  //   it("Shows User Data", async () => {
  //     // const set = db.ref().push().set;

  //     const wrapper = mount(<ProfileForm db={db} user={user} />);
  //     let tree = wrapper.find("label").first().text();
  //     expect(tree).toMatch("Name");
  //     console.log(wrapper);
  //     // expect(set).toHaveBeenCalledTimes(1);

  //     //     expect(set).toHaveBeenCalledWith({
  //     //       courseId: "THE_ROAD_TO_GRAPHQL",
  //     //       packageId: "STUDENT",
  //     //       invoice: {
  //     //         createdAt: "TIMESTAMP",
  //     //         amount: 0,
  //     //         licensesCount: 1,
  //     //         currency: "USD",
  //     //         paymentType: "FREE",
  //     //       },
  //     //     });
  //   });
});
