const { Builder, By } = require("selenium-webdriver");

describe("Get homepage", () => {
  test("it performs a validation of title on the home page", async () => {
    let driver = new Builder().forBrowser("firefox").build();
    try {
      await driver.get("http://localhost:3001"); // FIXME
      const title = await driver.findElement(By.css("h1")).getText();
      expect(title).toContain("Rockburn Online Booking System");
    } finally {
      await driver.quit();
    }
  });
});
