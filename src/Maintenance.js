import React from "react";

function Maintenance() {
  return (
    <div>
      <div className="header">
        <h1>Rockburn Online Booking System</h1>
      </div>
      <h1>Maintenance</h1>
      <div style={{ margin: "100px" }}>
        <p>Site offline for upgrade</p>
        <p>
          Sorry for the interruption but I need to make some data changes
          without anyone else on the site.
        </p>
        <p>Back soon</p>
      </div>
    </div>
  );
}

export default Maintenance;
