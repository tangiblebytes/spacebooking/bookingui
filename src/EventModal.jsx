import React, { useState, useEffect } from "react";

import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Spinner from "react-bootstrap/Spinner";
import Form from "react-bootstrap/Form";
// import { Link } from "react-router-dom";

import moment from "moment";
import "firebase/firestore";
import "firebase/functions";

function EventModal(props) {
  const show = props.show;
  const setShow = props.setShow;
  const db = props.db;
  const updateCalender = props.updateCalender;
  const eventMode = props.eventMode;
  const event = props.bookingEvent;
  const [savingStatus, setSavingStatus] = useState(false);

  const [eventDate, setEventDate] = useState(new Date());
  const [title, setTitle] = useState("");
  const [capacity, setCapacity] = useState(0);
  const [startTime, setStartTime] = useState("15:00");
  const [endTime, setEndTime] = useState("16:00");

  useEffect(() => {
    if (props.eventMode === "add") {
      setStartTime(moment.utc(props.date).format("HH:mm"));
      setEndTime(moment.utc(props.date).add(1, "hours").format("HH:mm"));
      setEventDate(moment.utc(props.date).format("YYYY-MM-DD"));
      setTitle("climbing");
      setCapacity(15);
    } else {
      setStartTime(moment.utc(props.bookingEvent.start).format("HH:mm"));
      setEndTime(moment.utc(props.bookingEvent.end).format("HH:mm"));
      setEventDate(moment.utc(props.bookingEvent.start).format("YYYY-MM-DD"));
      setTitle(props.bookingEvent.extendedProps.rawTitle);
      setCapacity(props.bookingEvent.extendedProps.capacity);
    }
  }, [props]);

  let addtoDB = (newEvent) => {
    return db.collection("events").add(newEvent);
  };
  let savetoDB = (newEvent) => {
    return db.collection("events").doc(event.id).set(newEvent);
  };
  let saveEvent = (action) => {
    setSavingStatus(true);
    action({
      start: new Date(eventDate + " " + startTime),
      end: new Date(eventDate + " " + endTime),
      booked: 0,
      capacity: capacity,
      title: title,
    })
      .then(() => {
        updateCalender();
        setSavingStatus(false);
        setShow(false);
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
  };

  let modalTitle = eventMode === "add" ? "New Event" : "Edit Event";
  let actionButton;
  if (eventMode === "add") {
    actionButton = (
      <Button variant="primary" onClick={() => saveEvent(addtoDB)}>
        Add Event
      </Button>
    );
  } else {
    actionButton = (
      <Button variant="primary" onClick={() => saveEvent(savetoDB)}>
        {" "}
        Save Event
      </Button>
    );
  }
  return (
    <Modal
      show={show}
      onHide={() => {
        setSavingStatus(false);
        setShow(false);
      }}
    >
      <Modal.Header closeButton>
        <Modal.Title>{modalTitle}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="date">
            <Form.Label>Date</Form.Label>
            <Form.Control
              value={eventDate}
              type="date"
              onChange={(e) => {
                setEventDate(e.target.value);
              }}
            />
            <Form.Text>
              {moment.utc(eventDate).format("dddd MMMM Do")}
            </Form.Text>
          </Form.Group>
          <Form.Group controlId="title">
            <Form.Label>Title</Form.Label>
            <Form.Control
              value={title}
              type="text"
              onChange={(e) => setTitle(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="start">
            <Form.Label>Start Time</Form.Label>
            <Form.Control
              value={startTime}
              type="time"
              onChange={(e) => setStartTime(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="end">
            <Form.Label>End Time</Form.Label>
            <Form.Control
              value={endTime}
              type="time"
              onChange={(e) => setEndTime(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="capacity">
            <Form.Label>Capacity</Form.Label>
            <Form.Control
              value={capacity}
              type="number"
              onChange={(e) => setCapacity(e.target.value)}
            />
            <Form.Text className="text-muted">
              This is the maximum number of people who will be allowed to book
              for the event.
            </Form.Text>
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        {savingStatus ? (
          <Spinner animation="border" variant="primary" />
        ) : (
          <>
            <Button
              variant="secondary"
              onClick={() => {
                setSavingStatus(false);
                setShow(false);
              }}
            >
              Cancel
            </Button>
            {actionButton}
          </>
        )}
      </Modal.Footer>
    </Modal>
  );
}

export default EventModal;
