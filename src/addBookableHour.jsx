import moment from "moment";
import * as Sentry from "@sentry/react";

function addBookableHour(bookingTime, db, statusHolder) {
  const docname = moment(bookingTime).utcOffset(0).format("YYYY-MM-DD");
  var bookingRef = db.collection("bookings").doc(docname);
  return (
    db
      .runTransaction((transaction) => {
        // This code may get re-run multiple times if there are conflicts.
        return transaction.get(bookingRef).then((hourDoc) => {
          // console.log(hourDoc);
          if (hourDoc.exists) {
            return Promise.reject("This hour is already bookable");
          }

          // create new object
          let date = moment(bookingTime).utcOffset(0);
          date.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
          const emptyBookingObject = {
            week: moment(bookingTime).format("W"),
            year: bookingTime.getUTCFullYear(),
            month: bookingTime.getUTCMonth() + 1,
            date: date.toDate(),
            hours: new Array(24).fill(0),
          };

          return transaction.set(bookingRef, emptyBookingObject, {
            merge: false,
          });
        });
      })
      // .then((arg) => {
      //   console.log("then 2");
      //   console.log(arg);
      // })
      .catch((err) => {
        Sentry.captureException(err);
        console.error(err);
        //   console.log(err.name);
        //   console.log(err.message);
        //   console.log(err.stack);

        return null;
      })
  );
}

export default addBookableHour;
