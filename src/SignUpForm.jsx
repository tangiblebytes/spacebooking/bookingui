import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
//import firebase from "firebase/app";
// import * as firebase from "firebase/app";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";

function SignUpForm(props) {
  const auth = props.auth;

  const history = useHistory();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [showErrorMessage, setShowErrorMessage] = useState(false);

  let handleSubmit = (event) => {
    if (password !== password2) {
      setErrorMessage("Passwords do not match.");
      setShowErrorMessage(true);
    } else {
      auth
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          history.push({ pathname: "/profile/edit" });
        })
        .catch(function (error) {
          // Handle Errors here.
          if (error.code && error.message) {
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode === "auth/weak-password") {
              setErrorMessage("The password is too weak.");
              setShowErrorMessage(true);
            } else {
              setErrorMessage(errorMessage);
              setShowErrorMessage(true);
            }
          } else {
            setErrorMessage("Unexpected error on signup");
            setShowErrorMessage(true);
          }
        });
    }
    event.preventDefault();
  };

  return (
    <div className="container">
      <Alert show={showErrorMessage} variant="warning">
        {errorMessage}
      </Alert>
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="signUpFormEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            placeholder=""
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>
        <Form.Group controlId="signUpFormPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder=""
            required
            value={password}
            minLength="8"
            onChange={(e) => setPassword(e.target.value)}
          />
          <Form.Text>Minimum password length is 8 characters</Form.Text>
        </Form.Group>
        <Form.Group controlId="signConfirmPassword">
          <Form.Label>Confirm Password</Form.Label>
          <Form.Control
            type="password"
            placeholder=""
            required
            value={password2}
            minLength="8"
            onChange={(e) => setPassword2(e.target.value)}
          />
        </Form.Group>

        <p>
          <Button variant="primary" type="submit">
            Create Account
          </Button>{" "}
          <Button variant="secondary" href="/">
            Cancel
          </Button>
        </p>
      </Form>
      <p>&nbsp;</p>
      <p>
        <Link to="/reset">Request Password Reset</Link>{" "}
      </p>
    </div>
  );
}

export default SignUpForm;
