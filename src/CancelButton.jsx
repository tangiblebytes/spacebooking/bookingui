import moment from "moment";
import "firebase/firestore";
// import * as firebase from "firebase/app";
// import firebase from "firebase/app";
import React, { useState } from "react";
import Spinner from "react-bootstrap/Spinner";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Alert from "react-bootstrap/Alert";

function CancelButton(props) {
  const bookingID = props.bookingID;
  const bookingName = props.bookingName;
  const bookingDate = props.bookingDate;
  const functions = props.functions;
  const [status, setStatus] = useState("default");
  const [errorMessage, setErrorMessage] = useState("");
  const doCancel = () => {
    setStatus("updating");

    const doCancelBooking = functions.httpsCallable("doCancelBooking");

    doCancelBooking({ bookingID: bookingID })
      // .then((result) => {
      //   setLastBooking(Date.now()); // trigger update of my bookings
      //   updateCalender();
      // })
      .then(() => {
        setStatus("cancelled");
      })
      .catch((error) => {
        setStatus("error");
        setErrorMessage(error.message);
      });
  };

  if (status === "updating") {
    return <Spinner animation="border" variant="primary" />;
  } else if (status === "cancelled") {
    return <p>Cancelled</p>;
  } else if (status === "error") {
    return <Alert variant="danger">{errorMessage}</Alert>;
  } else if (status === "dialog") {
    return (
      <Modal
        show
        onHide={() => {
          setStatus("default");
        }}
      >
        <Modal.Header closeButton>
          <Modal.Title>Confirm Cancel</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Cancel Booking for {bookingName} :{" "}
          {moment.utc(bookingDate).format("MMMM Do ha")}
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => {
              setStatus("default");
            }}
          >
            Abort
          </Button>
          <Button variant="primary" onClick={doCancel}>
            Cancel Booking
          </Button>
        </Modal.Footer>
      </Modal>
    );
  } else {
    return (
      <Button
        onClick={() => {
          setStatus("dialog");
        }}
      >
        Cancel
      </Button>
    );
  }
}

export default CancelButton;
