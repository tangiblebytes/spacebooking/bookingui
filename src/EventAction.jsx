import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Spinner from "react-bootstrap/Spinner";
import Alert from "react-bootstrap/Alert";
import { Link } from "react-router-dom";
import moment from "moment";
// import firebase from "firebase/app";

function EventAction(props) {
  const show = props.show;
  const setShow = props.setShow;
  const updateCalender = props.updateCalender;
  const bookingEvent = props.event;
  const myBookings = props.myBookings;
  const setLastBooking = props.setLastBooking;
  const user = props.user;
  const isAdmin = props.isAdmin;
  const showEditEvent = props.showEditEvent;
  const functions = props.functions;
  const [savingStatus, setSavingStatus] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const saveChanges = (action) => {
    const remoteMethod = functions.httpsCallable(action);
    setSavingStatus(true);
    setErrorMessage("");
    remoteMethod({ eventID: bookingEvent.id })
      .then((result) => {
        setLastBooking(Date.now()); // trigger update of my bookings
        updateCalender();
      })
      .then(() => {
        setShow(false);
        setSavingStatus(false);
      })
      .catch((error) => {
        setSavingStatus(false);
        setErrorMessage(error.message);
      });
  };

  const handleClose = () => {
    setShow(false);
  };
  let alreadyBooked = "";
  if (myBookings.includes(bookingEvent.id)) {
    alreadyBooked = `You already have one or more bookings for this session.
      Continue to book an additional place`;
  }
  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>{bookingEvent.extendedProps.rawTitle}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {moment.utc(bookingEvent.start).format("MMMM Do ha")}
        <p>{alreadyBooked}</p>

        {isAdmin && (
          <div>
            {" "}
            <p>
              <Button
                variant="primary"
                onClick={() => {
                  showEditEvent();
                }}
              >
                Edit Event
              </Button>
            </p>
            <p>
              <Button
                variant="danger"
                onClick={() => saveChanges("doCancelEvent")}
              >
                Cancel Event
              </Button>
            </p>
          </div>
        )}
      </Modal.Body>
      <Modal.Footer>
        {user ? (
          savingStatus ? (
            <Spinner animation="border" variant="primary" />
          ) : errorMessage !== "" ? (
            <Alert variant="danger">{errorMessage}</Alert>
          ) : (
            <>
              <p>
                {" "}
                <Button
                  variant="primary"
                  onClick={() => saveChanges("doBooking")}
                >
                  Book
                </Button>
              </p>
              <Button variant="secondary" onClick={() => handleClose()}>
                Cancel
              </Button>
            </>
          )
        ) : (
          <div>
            <Link to="/signin">Log in</Link> or{" "}
            <Link to="/register">Register</Link> to book a session.
          </div>
        )}
      </Modal.Footer>
    </Modal>
  );
}

export default EventAction;
