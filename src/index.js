import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as Sentry from "@sentry/react";
// import { Integrations } from "@sentry/tracing";
import App from "./App";
import "./main.scss";
// import Maintenance from "./Maintenance";

import * as serviceWorker from "./serviceWorker";

import firebase from "firebase/app";
import "firebase/auth";

import stagingFirebaseConfig from "./stagingFirebaseConfig";

import prodFirebaseConfig from "./prodFirebaseConfig";

let firebaseConfig = prodFirebaseConfig;
if (
  process.env.NODE_ENV === "development" ||
  process.env.REACT_APP_ENV === "staging"
) {
  firebaseConfig = stagingFirebaseConfig;
}

// disable sometimes during dev
Sentry.init({
  release: "rockburn-booking@" + process.env.npm_package_version,
  dsn:
    "https://03b21d3866b8458da2d39dd42e454779@o481474.ingest.sentry.io/5530206",
  // integrations: [new Integrations.BrowserTracing()],
  // environment: process.env.NODE_ENV,
  // // We recommend adjusting this value in production, or using tracesSampler
  // // for finer control
  // tracesSampleRate: 1.0,
});
// const functions = require("firebase-functions");

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth();
const functions = firebase.app().functions("europe-west2");

if (window.location.hostname === "localhost") {
  db.settings({
    host: "localhost:8080",
    ssl: false,
  });
  auth.useEmulator("http://localhost:9099/");
  functions.useEmulator("localhost", 5001);
}
ReactDOM.render(
  <React.StrictMode>
    <App auth={auth} db={db} functions={functions} />
    {/* <Maintenance /> */}
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
