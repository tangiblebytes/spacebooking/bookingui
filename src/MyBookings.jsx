import React, { useState, useEffect } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import Button from "react-bootstrap/Button";

import CancelButton from "./CancelButton";
import moment from "moment";

function MyBookings(props) {
  const db = props.db;
  const user = props.user; //in App state
  const functions = props.functions;
  // const profile = props.profile; //in App state
  const [bookings, setBookings] = useState([]);
  const [past, setPast] = useState(false);

  useEffect(() => {
    if (user) {
      const startOfToday = new Date(new Date().setHours(0, 0, 0, 0));
      let searchOption = past ? "<" : ">";
      let data = [];
      db.collection("userBookings")
        .where("booking.uid", "==", user.uid)
        .where("booking.date", searchOption, startOfToday)
        .get()
        .then(function (querySnapshot) {
          querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            let date = doc.data().booking.date.toDate();
            data.push({
              bookingID: doc.id,
              eventID: doc.data().eventID,
              timestamp: moment(date).format("YYYY-MM-DD-HH-mm"),
              date: moment(date).format("dddd Do MMMM YYYY"),
              time: moment(date).format("h:mm a"),
              dateDate: date,
              cancelled: doc.data().cancelled,
            });
          });
          return data;
        })
        .then((data) => {
          setBookings(data);
        })
        .catch(function (error) {
          console.log("Error getting documents: ", error);
        });
    }
  }, [user, db, past]);

  const columns = [
    {
      dataField: "dateDate",
      hidden: true,
    },
    {
      dataField: "timestamp",
      text: "Date",
      formatter: (cell, row, rowIndex) => {
        return row.date; // sort by timestamp but display date
      },
      sort: true,
    },
    {
      dataField: "date",

      hidden: true,
    },
    {
      text: "Time",
      dataField: "time",
    },
    {
      dataField: "cancelled",
      text: "Cancelled",
      hidden: true,
    },
    {
      dataField: "id",
      text: "Cancel",
      sort: false,
      formatter: (cell, row, rowIndex) => {
        return row.cancelled ? (
          "Cancelled"
        ) : (
          <CancelButton
            functions={functions}
            bookingID={row.bookingID}
            bookingName="
            "
            bookingDate={row.dateDate}
          />
        );
      },
    },
  ];
  let pastFuture = past ? "Show Future Bookings" : "Show Past Bookings";
  return (
    <div className="container">
      <BootstrapTable
        bootstrap4
        keyField="id"
        data={bookings}
        columns={columns}
        sort={{ dataField: "timestamp", order: "desc" }}
      />

      <p>
        <Button
          onClick={() => {
            setPast(!past);
          }}
        >
          {pastFuture}
        </Button>
      </p>
    </div>
  );
}

// cancelBooking(db, bookingDate, bookingID, userName)
export default MyBookings;
