import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/Alert";

function PasswordResetForm(props) {
  const auth = props.auth;
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [variant, setVariant] = useState("warning");
  const [showMessage, setShowMessage] = useState(false);

  const showAlert = (newMessage, newVariant) => {
    setVariant(newVariant);
    setMessage(newMessage);
    setShowMessage(true);
  };

  const sendResetEmail = (event) => {
    var actionCodeSettings = {
      // link for continue button after password reset
      url: "https://rockburn-booking.web.app/",
      handleCodeInApp: false,
    };

    var emailAddress = email;

    auth
      .sendPasswordResetEmail(emailAddress, actionCodeSettings)
      .then(function () {
        showAlert(
          "A reset email has been sent. Please check your mailbox.",
          "success"
        );
      })
      .catch(function (error) {
        showAlert(error.message, "warning");
      });

    event.preventDefault();
  };

  return (
    <div className="container">
      <Alert show={showMessage} variant={variant}>
        {message}
      </Alert>
      <Form onSubmit={sendResetEmail}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            required
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Request Password Reset
        </Button>
      </Form>
    </div>
  );
}

export default PasswordResetForm;
