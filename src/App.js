import React, { useState, useEffect } from "react";

import BookingApp from "./BookingApp";
import SignInForm from "./SignInForm";
import SignUpForm from "./SignUpForm";
import PasswordResetForm from "./PasswordResetForm";
import ProfileForm from "./ProfileForm";
import getProfile from "./Profile";
import MyBookings from "./MyBookings";
import MemberList from "./MemberList";
import ViewBookings from "./ViewBookings";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import * as Sentry from "@sentry/react";

// import Test from "./Test";

function App(props) {
  const db = props.db;
  const auth = props.auth;
  const functions = props.functions;
  const blankProfile = { name: "" };

  const [user, setUser] = useState(null);
  const [profile, setProfile] = useState(blankProfile);
  let error = "";

  useEffect(() => {
    const authListener = auth.onAuthStateChanged((authUser) => {
      // console.log(authUser);
      // console.log(authUser.uid);

      if (authUser) {
        setUser(authUser);
        getProfile(db, authUser)
          .then((profile) => {
            //console.log(JSON.stringify(profile));
            setProfile(profile);
          })
          .catch((error) => {
            console.log(error);
          });

        //   setAuthWasListened(true);
      } else {
        setUser(null);
        //   setAuthWasListened(true);
      }
    });
    return authListener; // THIS MUST BE A FUNCTION, AND NOT A FUNCTION CALL
  }, [db, auth]);

  const signOut = () => {
    auth
      .signOut()
      .then(function () {
        setUser(null);
        setProfile(blankProfile);
      })
      //.then(window.location.reload())
      .catch(function (error) {
        this.error = error;
      });
  };

  function fallback() {
    return <p>Oops.</p>;
  }

  return (
    <>
      <Sentry.ErrorBoundary showDialog fallback={fallback}>
        <Router>
          <div className="header">
            <h1>
              <Link to="/">Rockburn Online Booking System</Link>
            </h1>
          </div>
          <div className="container">
            <p>{error}</p>
            {user ? (
              <p>Hello, {profile.name}</p>
            ) : (
              <p>
                <Link to="/signin">sign in</Link> |{" "}
                <Link to="/register">register</Link>
              </p>
            )}
            {user ? (
              <p>
                {" "}
                <Link to="/">home</Link> |
                <Link to="/" onClick={signOut}>
                  {" "}
                  sign out
                </Link>{" "}
                | <Link to="/profile/edit">my account</Link> |{" "}
                <Link to="/profile/bookings">my bookings</Link>
              </p>
            ) : (
              <p></p>
            )}
            {profile.isAdmin && (
              <p>
                {" "}
                Admin : <Link to="/admin/bookings">bookings</Link> |{" "}
                <Link to="/admin/members">members</Link>
              </p>
            )}
          </div>
          <div>
            {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
            <Switch>
              <Route path="/register">
                <SignUpForm auth={auth} />
              </Route>
              <Route path="/signin">
                <SignInForm auth={auth} />
              </Route>
              <Route path="/reset">
                <PasswordResetForm auth={auth} />
              </Route>
              <Route path="/profile/edit">
                <ProfileForm db={db} user={user} profile={profile} />
              </Route>
              <Route path="/profile/bookings">
                <MyBookings
                  db={db}
                  functions={functions}
                  user={user}
                  profile={profile}
                />
              </Route>
              <Route path="/admin/members">
                <MemberList db={db} user={user} />
              </Route>
              <Route path="/admin/bookings">
                <ViewBookings
                  db={db}
                  functions={functions}
                  user={user}
                  profile={profile}
                />
              </Route>
              <Route path="/">
                <BookingApp
                  db={db}
                  functions={functions}
                  user={user}
                  profile={profile}
                />
                {/* <Test db={db} /> */}
              </Route>
            </Switch>
          </div>
        </Router>
      </Sentry.ErrorBoundary>
    </>
  );
}

export default App;
