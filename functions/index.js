// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require("firebase-functions");

var admin = require("firebase-admin");
let env = functions.config().env.name;

var serviceAccount = require(`./${env}-serviceAccountKey.json`);

const databaseURL =
  env === "staging"
    ? "https://rockburn-staging.firebaseio.com"
    : "https://rockburn-booking.firebaseio.com";

const firestore = require("@google-cloud/firestore");
// const {
//   exampleDocumentSnapshotChange,
// } = require("firebase-functions-test/lib/providers/firestore");
const client = new firestore.v1.FirestoreAdminClient();

//console.log(serviceAccount);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: databaseURL,
});

const db = admin.firestore();
// The Firebase Admin SDK to access Cloud Firestore.
//const admin = require("firebase-admin");
//admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions
  .region("europe-west2")
  .https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
  });

// Take the text parameter passed to this HTTP endpoint and insert it into
// Cloud Firestore under the path /messages/:documentId/original
// exports.addMessage = functions.region('europe-west2').https.onRequest(async (req, res) => {
//   // Grab the text parameter.
//   const original = req.query.text;
//   // Push the new message into Cloud Firestore using the Firebase Admin SDK.
//   const writeResult = await admin
//     .firestore()
//     .collection("messages")
//     .add({ original: original });
//   // Send back a message that we've succesfully written the message
//   res.json({ result: `Message with ID: ${writeResult.id} added.` });
// });

// eg request like GET http://localhost:5001/rockburn-booking/us-central1/addMessage?text=foo will write
// in collection messages - document id random -  { original : "foo" }

// exports.listAllUsers = functions.region('europe-west2').https.onRequest((request, response) => {
//   if (process.env.FUNCTIONS_EMULATOR !== "true") {
//     response.send("Only Available Via Emulator");
//     return null;
//   }

//   admin
//     .auth()
//     .listUsers(1000)
//     .then((listUsersResult) => {
//       var users = [];
//       listUsersResult.users.forEach((userRecord) => {
//         //console.log("user", userRecord.toJSON());
//         users.push(userRecord);
//       });
//       // console.log(JSON.stringify(users));

//       response.send(users);
//       return null;
//     })
//     .catch((error) => {
//       console.log("Error listing users:", error);
//     });
//   return null;
// });

//

// exports.bookingHoursToEvents = functions.region('europe-west2').https.onRequest(
//   (request, response) => {
//     //get opeing hours
//     const moment = require("moment");
//     db.collection("businessHours")
//       .get()
//       .then((querySnapshot) => {
//         let allEvents = [];
//         querySnapshot.forEach((doc) => {
//           let weekString = doc.id;
//           if (weekString.length !== 7 || weekString.substr(5, 2) < 45) {
//             console.log(weekString);
//             return;
//           }
//           let week = doc.data().hours;
//           week.forEach((day) => {
//             let weekday = day.daysOfWeek[0] - 1;
//             if (weekday === -1) {
//               weekday = 6;
//             }
//             let year = weekString.substr(0, 4);
//             let week = weekString.substr(5, 2);
//             let today = moment()
//               .year(year)
//               .week(week)
//               .day("monday")
//               .hours("00")
//               .minutes("00")
//               .seconds("00")
//               .millisecond(0);
//             today.add(weekday, "days");
//             let start_time = parseInt(day.startTime);
//             let end_time = parseInt(day.endTime);
//             let events = [];
//             for (let start = start_time; start < end_time; start++) {
//               let start_date = new moment(today);
//               start_date.add(start, "hours");
//               let end_date = new moment(start_date);
//               end_date.add(1, "hours");
//               events.push({
//                 date: today.toDate(),
//                 start: start_date.toDate(),
//                 end: end_date.toDate(),
//                 capacity: 15,
//                 booked: 0,
//                 title: "climbing",
//                 color: "green",
//                 week: weekString,
//               });
//             }

//             allEvents = allEvents.concat(events);
//           });
//         });
//         // console.log(events);
//         response.json(allEvents);
//         return allEvents;
//       })
//       .then((allEvents) => {
//         var batch = db.batch();
//         allEvents.forEach((event) => {
//           let newDocRef = db.collection("events").doc();
//           batch.set(newDocRef, event);
//         });
//         return batch.commit();
//       })
//       .then(() => {
//         console.log("Document successfully written!");
//         response.status(200).end();
//         return;
//       })
//       .catch((error) => {
//         console.error("Error getting document:", error);
//         response.status(500).end();
//       });
//   }
// );

// exports.test = functions.region('europe-west2').https.onRequest((request, response) => {
//   let events = [];
//   const moment = require("moment");

//   let date = moment("2020-12-03 18:00:00");
//   events.push(date.toDate());
//   db.collection("events")
//     .where("start", "==", date.toDate())
//     .get()
//     .then((querySnapshot) => {
//       var batch = db.batch();
//       let batchCount = 0;
//       querySnapshot.forEach((event) => {
//         events.push(event.data().start.toDate());
//       });
//       response.json(events);
//       response.status(200).end();
//       return;
//     })
//     .catch((e) => {
//       response.json(e);
//       response.status(500).end();
//     });
// });

// exports.updateBookingDefaults = functions.region('europe-west2').https.onRequest(
//   (request, response) => {
//     let data = {};
//     db.collection("userBookings")
//       .get()
//       .then((querySnapshot) => {
//         let eventPromises = [];
//         var batch = db.batch();
//         let batchCount = 0;

//         querySnapshot.forEach((doc) => {
//           let cancelled = false;
//           if (doc.data().cancelled) {
//             cancelled = true;
//           }
//           data[doc.id] = doc.data();
//           let newDocRef = db.collection("userBookings").doc(doc.id);
//           batch.update(newDocRef, { eventID: "", cancelled: cancelled });
//           batchCount++;
//           if (batchCount >= 500) {
//             batch.commit();
//             batchCount = 0;
//             batch = db.batch();
//           }
//         });
//         if (batchCount > 0) {
//           batch.commit();
//         }

//         return;
//       })
//       .then(() => {
//         response.json(data);
//         response.status(200).end();
//         return;
//       })
//       .catch((e) => {
//         console.error("Error :", e);
//         response.json(e);
//         response.status(500).end();
//       });
//   }
// );

// exports.matchBookingsEvents = functions.region('europe-west2').https.onRequest((request, response) => {
//   const moment = require("moment");
//   // get booked hours
//   let bookings = {};
//   db.collection("userBookings")
//     .where("booking.date", ">", new Date())
//     .get()
//     .then((querySnapshot) => {
//       let eventPromises = [];
//       querySnapshot.forEach((doc) => {
//         if (doc.data().cancelled) {
//           return;
//         }
//         let start = moment(doc.data().booking.date.toDate());
//         let startString = start.format("YYYY-MM-DD HH:mm:ss");
//         bookings[startString] = { id: doc.id, data: doc.data() };
//         console.log(doc.data().booking.date.toDate());
//         eventPromises.push(
//           db
//             .collection("events")
//             .where("start", "==", doc.data().booking.date.toDate())
//             .get()
//         ); // at migration time only one event per timeslot
//       });
//       return Promise.all(eventPromises);
//     })
//     .then((querySnapshot) => {
//       var batch = db.batch();
//       let batchCount = 0;
//       querySnapshot.forEach((event) => {
//         // console.log(event);
//         if (event.empty) {
//           console.log("missing event for some future booking");
//         } else {
//           event.forEach((eventDoc) => {
//             console.log(eventDoc.id);
//             var eventRef = db.collection("events").doc(eventDoc.id);
//             eventRef.update({
//               booked: firestore.FieldValue.increment(1),
//             });
//             // add event ID to userBooking
//             if (bookings.hasOwnProperty(eventDoc.data().start)) {
//               let booking = bookings[eventDoc.data().start];
//               let newDocRef = db.collection("userBookings").doc(booking.id);
//               batch.update(newDocRef, { eventID: eventDoc.id });
//               batchCount++;
//               if (batchCount > 500) {
//                 batch.commit();
//                 batchCount = 0;
//                 batch = db.batch();
//               }
//             }
//           });
//         }
//       });
//       if (batchCount > 0) {
//         batch.commit();
//       }
//       return;
//     })

//     .then(() => {
//       response.json(bookings);
//       response.end();
//       return;
//     })

//     .catch((error) => {
//       console.error("Error getting documents: ", error);
//       response.status(500).end();
//     });
// });

// https://firebase.google.com/docs/firestore/solutions/schedule-export
const bucket = "gs://rockburn-backup";

exports.scheduledFirestoreExport = functions.pubsub
  .schedule("every 24 hours")
  .onRun((context) => {
    const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;
    const databaseName = client.databasePath(projectId, "(default)");

    return client
      .exportDocuments({
        name: databaseName,
        outputUriPrefix: bucket,
        // Leave collectionIds empty to export all collections
        // or set to a list of collection IDs to export,
        // collectionIds: ['users', 'posts']
        collectionIds: [],
      })
      .then((responses) => {
        const response = responses[0];
        console.log(`Operation Name: ${response["name"]}`);
        return;
      })
      .catch((err) => {
        console.error(err);
        console.log(err);
        throw new Error("Export operation failed");
      });
  });

exports.populateAuthRoles = functions
  .region("europe-west2")
  .https.onRequest(async (req, res) => {
    if (!process.env["FUNCTIONS_EMULATOR"]) {
      return res
        .status(403)
        .send("ACCESS DENIED. This function is ONLY available via an emulator");
    }

    let role = {
      role: { isAdmin: true },
      name: "user1",
    };
    res.send("done");
    res.end();
    // return;
    return db.collection("roles").doc("user1").set(role);
  });

exports.populateAuthUsers = functions
  .region("europe-west2")
  .https.onRequest(async (req, res) => {
    if (!process.env["FUNCTIONS_EMULATOR"]) {
      return res
        .status(403)
        .send("ACCESS DENIED. This function is ONLY available via an emulator");
    }
    const users = [
      {
        uid: "user1",
        displayName: "Sean Local Admin",
        email: "sean@test.com",
        password: "password",
      },
      {
        uid: "1LTeAjGO76ehLkgmyYulQI3VUAA3",
        displayName: "Sean Remote Data Admin",
        email: "sean@burlington.me.uk",
        password: "password",
      },
      // put all test users you want populated here
    ];

    const results = [];
    const promises = [];
    for (let user of users) {
      let promise = admin
        .auth()
        .createUser(user)
        .then((result) => {
          return result;
        })
        .catch((error) => {
          return error.message; // continue on errors (eg duplicate users)
        });

      promises.push(promise);
    }
    await Promise.all(promises).then((result) => {
      results.push(result);
      return;
    });
    res.header("Content-type", "application/json");
    return res.status(200).send(JSON.stringify(results));
  });

exports.doBooking = functions
  .region("europe-west2")
  .https.onCall((data, context) => {
    let uid = context.auth.uid;
    let name =
      context.auth.token.hasOwnProperty("name") &&
      context.auth.token.name !== ""
        ? context.auth.token.name
        : context.auth.token.email;
    let eventID = data.eventID;
    let eventTitle = "";
    let eventStart = {};
    if (!data.hasOwnProperty("eventID")) {
      throw new functions.https.HttpsError(
        "failed-precondition",
        "No EventID provided"
      );
    }
    var eventRef = db.collection("events").doc(eventID);

    let promise = db
      .runTransaction((transaction) => {
        // This code may get re-run multiple times if there are conflicts.
        return transaction.get(eventRef).then((eventDoc) => {
          if (eventDoc.exists) {
            let event = eventDoc.data();
            eventTitle = event.title;
            eventStart = event.start.toDate();
            if (event.booked < event.capacity) {
              // var eventRef = db.collection("events").doc(eventDoc.id);
              eventRef.update({
                booked: firestore.FieldValue.increment(1),
              });

              return;
            } else {
              throw new functions.https.HttpsError(
                "resource-exhausted",
                "Event Fully Booked."
              );
            }
          } else {
            throw new functions.https.HttpsError(
              "failed-precondition",
              "Event not found"
            );
          }
        });
      })
      .then(() => {
        let booking = {
          eventID: eventID,
          cancelled: false,
          booking: {
            name: name,
            uid: uid,
            created: firestore.FieldValue.serverTimestamp(),
            eventTitle: eventTitle,
            date: eventStart,
          },
        };

        // return;
        return db.collection("userBookings").add(booking);
      })
      .catch((error) => {
        functions.logger.warn("Cancel Event - error", {
          eventID: eventID,
          error: error,
        });
        throw new functions.https.HttpsError("unknown", "Something went wrong");
      });

    return promise;
  });
/////////////////////////////////////////////////////////////////

exports.doCancelBooking = functions
  .region("europe-west2")
  .https.onCall((data, context) => {
    let uid = context.auth.uid;
    let name =
      context.auth.token.hasOwnProperty("name") &&
      context.auth.token.name !== ""
        ? context.auth.token.name
        : context.auth.token.email;
    if (!data.hasOwnProperty("bookingID")) {
      throw new functions.https.HttpsError(
        "failed-precondition",
        "No bookingID provided"
      );
    }
    let bookingID = data.bookingID;
    let eventID = null;
    // let delay = (ms) => {
    //   console.log("start delay");
    //   return new Promise((resolve) => setTimeout(resolve, ms));
    // };

    let promise = db
      .collection("userBookings")
      .doc(bookingID)
      .get()
      .then((bookingDoc) => {
        if (!bookingDoc.exists) {
          throw new functions.https.HttpsError(
            "not-found",
            "This booking doesn't exist!"
          );
        }

        let userBooking = bookingDoc.data();

        if (userBooking.cancelled) {
          throw new functions.https.HttpsError(
            "failed-precondition",
            "Already Cancelled"
          );
        }
        eventID = userBooking.eventID;
        return;
      })
      .then(() => {
        return db.collection("events").doc(eventID).get();
      })
      .then((eventDoc) => {
        if (!eventDoc.exists) {
          functions.logger.warn(
            "Cancel booking - booking found but not the event",
            { bookingID: bookingID, eventID: eventID }
          );
          return; // This shouldn't happen but even if we can't find the event still cancel the booking !
        }
        let eventRef = db.collection("events").doc(eventID);

        if (eventDoc.data().booked < 1) {
          functions.logger.warn(
            "Cancelling booking but booking number was already < 1 ",
            { bookingID: bookingID, eventID: eventID, event: eventDoc.data() }
          );

          eventRef.update({
            booked: 0,
          });
        } else {
          eventRef.update({
            booked: firestore.FieldValue.increment(-1),
          });
        }
        return;
      })

      .then(() => {
        var bookingRef = db.collection("userBookings").doc(bookingID);
        return bookingRef.update({
          cancelled: true,
          cancelTime: firestore.FieldValue.serverTimestamp(),
          cancelledBy: name,
        });
      })
      .then(() => {
        return { text: "OK" };
      });

    return promise;
  });

//////////////////////////////////////////////////////////////////////////////

exports.doCancelEvent = functions
  .region("europe-west2")
  .https.onCall((data, context) => {
    // let uid = context.auth.uid;
    let name =
      context.auth.token.hasOwnProperty("name") &&
      context.auth.token.name !== ""
        ? context.auth.token.name
        : context.auth.token.email;
    if (!data.hasOwnProperty("eventID")) {
      throw new functions.https.HttpsError(
        "failed-precondition",
        "No EventID provided"
      );
    }
    let eventID = data.eventID;
    let eventRef = db.collection("events").doc(eventID);
    let promise = eventRef
      .update({
        booked: 0,
        capacity: 0,
        start: new Date(0),
        end: new Date(0),
        cancelled: true,
        cancelTime: firestore.FieldValue.serverTimestamp(),
        cancelledBy: name,
      })
      .then(() => {
        return db
          .collection("userBookings")
          .where("eventID", "==", eventID)
          .get();
      })

      .then((querySnapshot) => {
        let cancelations = [];
        querySnapshot.forEach((userBooking) => {
          userBookingRef = db.collection("userBookings").doc(userBooking.id);
          cancelations.push(
            userBookingRef.update({
              cancelled: true,
              cancelTime: firestore.FieldValue.serverTimestamp(),
              cancelledBy: name,
            })
          );
        });
        return Promise.all(cancelations);
      })
      .catch((error) => {
        functions.logger.warn("Cancel Event - error", {
          eventID: eventID,
          error: error,
        });
        throw new functions.https.HttpsError("unknown", "Something went wrong");
      });
    return promise;
  });

exports.deleteStuff = functions
  .region("europe-west2")
  .https.onRequest(async (req, res) => {
    if (!process.env["FUNCTIONS_EMULATOR"]) {
      return res
        .status(403)
        .send("ACCESS DENIED. This function is ONLY available via an emulator");
    }
    let bookings = [];
    return (
      db
        .collection("events")
        // .where("booking.uid", "==", "XGd7ph8HvyOICDqUxNDjEPs3aT92")
        .get()
        .then((querySnapshot) => {
          return querySnapshot.forEach((doc) => {
            bookings.push(doc.id);
          });
        })
        .then(() => {
          bookings.forEach((bookingID) => {
            db.collection("events").doc(bookingID).delete();
          });
          res.json(bookings);
          res.status(200).end();
          return;
        })
        .catch((error) => {
          res.json(error);
          res.status(500).end();
        })
    );
  });
