# RockBurn Booking App

A Fairly minimal web app to allow people to book in at the RockBurn climbing wall as required under Covid-19 rules

All based on node 12 (node 14 is not yet supported in firebase functions)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=tangiblebytes_bookingui)

## Local Development

First run `firebase emulators:start --import=./emulator-data/` in another terminal (keep an eye on this for error logs)

Create local admin user via http://localhost:5001/rockburn-booking/us-central1/populateAuthUsers

Then `npm start`

Open http://localhost:3000 to view it in the browser or http://localhost:3001 if port 3000 is used elsewhere

To save a data snapshot run `firebase emulators:export emulator-data/`

## Test

Some automated testing added

To run all tests
`npm test`

Only "unit" tests
`npm test unit`

Unit tests are run on Gitlab CI

Only Selenium
`npm test selenium`

To run selenium tests you will need to [download](https://www.selenium.dev/downloads/) and run selenium server

```bash
java -jar selenium-server-standalone-3.141.59.jar
```

Also download [the latest driver](https://github.com/mozilla/geckodriver/releases) and put it on your path

To publish a preview URL for Customer Acceptance tests

```bash
firebase hosting:channel:deploy some-preview-name
```

This will publish and provide a new URL of new content loading from production data and auth

The url is valid until it expires (add eg `--expires 5d`) default is 7 days.

To update run the same command again.

## Build

`npm run build`

Builds the app for production to the `build` folder.

## Deploy

Console is at

https://console.firebase.google.com/u/0/project/rockburn-booking/overview

`firebase deploy`

For faster changes where no functions have been changed

`firebase deploy --only hosting`

## Database schema

The App uses [Firestore](https://firebase.google.com/docs/firestore)

### Collections

- bookings
- businessHours
- roles
- userBookings
- users

## Install / Update Firebase Tools

https://firebase.google.com/docs/cli#mac-linux-npm

```bash
# run anywhere
npm install -g firebase-tools ## NB this impacts the whole local system
# run from project directory
firebase init emulators
```

## Update Packages

```bash
npm update
npm audit
```

## See also

Config data with private API keys

https://gitlab.com/tangiblebytes/spacebooking/bookingui/-/snippets/2050744
https://gitlab.com/tangiblebytes/spacebooking/bookingui/-/snippets/2050688
